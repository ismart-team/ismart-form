<?php

namespace Ismart\Form;


use PHPMailer\PHPMailer\PHPMailer;

class Form
{
  private $data;
  private $render_engine;
  private $mail;
  private $config;
  private $template_name;

  public function __construct($config)
  {
    $this->config = $config;

    $this->template_name = "email.twig";

    if (array_key_exists("template", $config)) {
      $this->template_name = $config["template"];
    }

    $loader = new \Twig\Loader\FilesystemLoader($config["templates"]);
    $this->render_engine = new \Twig\Environment($loader);
    $this->render_engine->getExtension('Twig_Extension_Core')->setTimezone('Europe/Moscow');

    $this->mail = new PHPMailer();

    if (array_key_exists("smtp", $config)) {

      switch ($config["smtp"]) {
        case "yandex":
          $secure = "ssl";
          $host = "smtp.yandex.ru";
          $port = 465;
          break;
        case "gmail":
        default:
          $secure = "tls";
          $host = "smtp.gmail.com";
          $port = 587;
      }


      $this->mail->IsSMTP();
      $this->mail->SMTPAuth = true;
      $this->mail->SMTPSecure = $secure;
      $this->mail->Host = $host;
      $this->mail->Port = $port;
      $this->mail->Username = $config["email"];
      $this->mail->Password = $config["password"];
      $this->mail->SetFrom($config["email"], $_SERVER["HTTP_HOST"]);
    } else {
      $this->mail->SetFrom("no-reply@" . $_SERVER["HTTP_HOST"], $_SERVER["HTTP_HOST"]);
    }

    $this->mail->CharSet = "utf-8";



    // Добавляет скрытых получателей email
    if (array_key_exists("addressCC", $config)) {
      foreach ($config["addressCC"] as $item) {
        $this->mail->AddCC($item, "");
      }
    }
  }

  public function send($data, $errorCallback, $successCallback)
  {
    $form_data = $this->sanitize($data);

    $options["Адрес"] = $form_data['url'];
    $options["Дата"] = date("d.m.Y H:i:s");
    $options["Браузер"] = $_SERVER['HTTP_USER_AGENT'];

    if (array_key_exists("address", $form_data) && is_array($form_data["address"])) {
      foreach ($form_data["address"] as $item) {
        $this->mail->AddAddress($item, "");
      }
    } elseif (array_key_exists("address", $this->config) && is_array($this->config["address"])) {
      //Добавляет прямых получателей email
      foreach ($this->config["address"] as $item) {
        $this->mail->AddAddress($item, "");
      }
    }

    $this->mail->Subject = $form_data["subject"];

    // Добавляет вложения
    if (array_key_exists("attaches", $form_data)) {
      foreach ($form_data["attaches"] as $attach) {
        $this->mail->addAttachment($attach);
      }
    }

    unset($form_data["subject"]);
    unset($form_data["url"]);
    unset($form_data["attaches"]);
    unset($form_data["address"]);

    $body_message = $this->render_engine->render($this->template_name, [
      "fields" => $form_data,
      "options" => $options
    ]);

    $this->mail->MsgHTML($body_message);
    $result = $this->mail->send();

    if (!$result && is_callable($errorCallback)) {
      $errorCallback($this->mail->ErrorInfo);
      exit;
    }

    if (is_callable($successCallback)) {
      $successCallback($form_data);
    }

  }

  /**
   * Конвертирует небезопасные символы в UNICODE последовательность
   *
   * @param array $arr
   * @return array
   */
  private function sanitize($arr)
  {
    return array_map(function($field) {

      if (is_array($field)) {
        return $this->sanitize($field);
      }

      $trimed = trim($field);
      return filter_var($trimed, FILTER_SANITIZE_STRING);
    }, $arr);
  }

  /**
   * Рендерит шаблон с переданными данными
   *
   * @param string $template - Имя шаблона (без расширения файла)
   * @param array $data - Данные которые передадутся в движок рендера
   * @return string - HTML разметка
  */
  public function render($template, $data)
  {
    return $this->render_engine->render($template, $data);
  }
}
